package pos.machine;

import java.util.*;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);
        return renderReceipt(receipt);
    }

    public List<ReceiptItem> decodeToItems(List<String> barcodes) {
        List<ReceiptItem> receiptItems = new ArrayList<>();
        List<Item> items = ItemsLoader.loadAllItems();
        Map<String, Integer> map = new HashMap<>();
        barcodes.forEach(barcode -> {
            map.put(barcode, map.getOrDefault(barcode, 0) + 1);
        });
        items.forEach(item -> {
            String barcode = item.getBarcode();
            if (map.containsKey(barcode)) {
                receiptItems.add(new ReceiptItem(item.getName(), map.get(barcode), item.getPrice()));
            }
        });
        return receiptItems;
    }

    public List<ReceiptItem> calculateItemsCost(List<ReceiptItem> receiptItems) {
        receiptItems.forEach(receiptItem -> {
            receiptItem.setSubTotal(receiptItem.getQuantity() * receiptItem.getUnitPrice());
        });
        return receiptItems;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems) {
        return new Receipt(calculateItemsCost(receiptItems), calculateTotalPrice(receiptItems));
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItems) {
        int totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            totalPrice += receiptItem.getSubTotal();
        }
        return totalPrice;
    }

    public String renderReceipt(Receipt receipt) {
        String itemsReceipt = generateItemsReceipt(receipt);
        return generateReceipt(itemsReceipt, receipt.getTotalPrice());
    }

    public String generateReceipt(String itemsReceipt, int totalPrice) {
        return "***<store earning no money>Receipt***\n" +
                itemsReceipt +
                "----------------------\n" +
                "Total: "+ totalPrice +" (yuan)\n" +
                "**********************";
    }

    public String generateItemsReceipt(Receipt receipt) {
        List<ReceiptItem> receiptItems = receipt.getReceiptItems();
        StringBuilder itemsReceipt = new StringBuilder();
        receiptItems.forEach(receiptItem -> {
            itemsReceipt.append(generateItemReceipt(receiptItem));
            itemsReceipt.append("\n");
        });
        return itemsReceipt.toString();
    }

    public String generateItemReceipt(ReceiptItem receiptItem) {
        StringJoiner itemReceipt = new StringJoiner(", ");
        itemReceipt.add("Name: " + receiptItem.getName());
        itemReceipt.add("Quantity: " + receiptItem.getQuantity());
        itemReceipt.add("Unit price: " + receiptItem.getUnitPrice() + " (yuan)");
        itemReceipt.add("Subtotal: " + receiptItem.getSubTotal() + " (yuan)");
        return itemReceipt.toString();
    }

}
